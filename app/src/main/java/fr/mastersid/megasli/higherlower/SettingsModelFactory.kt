package fr.mastersid.megasli.higherlower

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SettingsModelFactory(private val context : Context?) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass : Class<T>): T {
        if(modelClass.isAssignableFrom(SettingsFragmentModel::class.java)){
            //val backend = FavoriteDummyBackEnd()
            val backend = FavoriteSharedPreferencesBackEnd(context)
            val repository = FavoriteRepository(backend)
            return SettingsFragmentModel(repository) as T
        }
        throw IllegalArgumentException("Unknown model class")
    }
}