package fr.mastersid.megasli.higherlower

import android.app.PendingIntent.getActivity
import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.google.android.material.internal.ContextUtils.getActivity
import java.security.AccessController.getContext

class SecretNumberModel(state : SavedStateHandle,private val context : Context ,private val max : Int, private var turns : Int) : ViewModel() {


    //val secretNumber = MutableLiveData(NO_SECRET_NUMBER)
    val secretNumber = state.getLiveData(SAVE_NUMBER, NO_SECRET_NUMBER)
    var remainingTurns = state.getLiveData(SAVE_TURNS,turns)
    //var remainingTurns = MutableLiveData(turns)
    fun chooseSecretNumber(){
        secretNumber.value = (1..max).random()
        remainingTurns.value = turns
        checkResult.value = CheckResult.NO_GUESS.message
        checkLost.value = CheckResult.NO_GUESS.message
    }
    enum class CheckResult(val message : String){ //used just to compare
        LOWER("The secret number is higher"), //the text that the application displays is in string.xml
        GREATER("The secret number is lower"),
        EQUAL("Well Done!"),
        NO_GUESS(" "),
        LOST("You've lost! the secret number was : ")
    }

    var checkResult = MutableLiveData(CheckResult.NO_GUESS.message)
    var checkLost = MutableLiveData(CheckResult.NO_GUESS.message)
    

    fun check(number : Int){
        if (number in 1..max && secretNumber.value in 1..max && remainingTurns.value!! > 0
                && checkResult.value != CheckResult.EQUAL.message) {

            if (number < secretNumber.value!!) {
                checkResult.value = CheckResult.LOWER.message
            } else if (number > secretNumber.value!!) {
                checkResult.value = CheckResult.GREATER.message
            } else if (number == secretNumber.value!!) {
                checkResult.value = CheckResult.EQUAL.message
            }
            remainingTurns.value = remainingTurns.value?.minus(1)
            if(remainingTurns.value == 0){
                checkLost.value = CheckResult.LOST.message
            }
        }

    }

    companion object{
        const val NO_SECRET_NUMBER= -1
        private const val SAVE_NUMBER = "DEFAULT_KEY"
        private const val SAVE_TURNS = "DEFAULT_KEY2"
        private const val SAVE_RESULT = "DEFAULT_KEY3"
    }

}