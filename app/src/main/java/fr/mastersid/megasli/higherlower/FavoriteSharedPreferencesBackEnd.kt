package fr.mastersid.megasli.higherlower

import android.content.Context
import android.util.Log

class FavoriteSharedPreferencesBackEnd(context : Context?):FavoriteBackEnd{
    private val sharedPreferences = context?.getSharedPreferences(FILE_NAME,Context.MODE_PRIVATE)
    override suspend fun saveFavorite(favorite: Int,){
        Log.d("Favorite ","saving...")
        sharedPreferences?.edit()?.apply{
            putInt(FAVORITE_VALUE, favorite)
            commit()
        }
        Log.d("Favorite","was saved")
    }

    override suspend fun saveFavorite2(favorite2: Int){
        Log.d("Favorite2 ","saving...")
        sharedPreferences?.edit()?.apply{
            putInt(FAVORITE_VALUE2,favorite2)
            commit()
        }
        Log.d("Favorite2","was saved")
    }

    override suspend fun loadFavorite():Int{
        Log.d("Favorite","Loading...")
        val favorite = sharedPreferences?.getInt(FAVORITE_VALUE,FavoriteBackEnd.NO_VALUE)?:FavoriteBackEnd.NO_VALUE
        Log.d("Favorite","Loaded")
        return favorite
    }

    override suspend fun loadFavorite2():Int{
        Log.d("Favorite2","Loading...")
        val favorite2 = sharedPreferences?.getInt(FAVORITE_VALUE2,FavoriteBackEnd.NO_VALUE)?:FavoriteBackEnd.NO_VALUE
        Log.d("Favorite2","Loaded")
        return favorite2
    }


    companion object{
        private const val FILE_NAME = "favorite_shared_preferences_file"

        private const val FAVORITE_VALUE = "favorite_value"
        private const val FAVORITE_VALUE2 = "favorite_value2"
    }
}