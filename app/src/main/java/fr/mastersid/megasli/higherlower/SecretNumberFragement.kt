package fr.mastersid.megasli.higherlower


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import fr.mastersid.megasli.higherlower.databinding.FragementSecretnumberBinding



class SecretNumberFragement : Fragment() {

        private lateinit var binding : FragementSecretnumberBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container : ViewGroup?,
                              savedInstanceState : Bundle?): View?{
        binding = FragementSecretnumberBinding.inflate(inflater)
        return binding.root

    }
    
    override fun onViewCreated(view: View, savedInstanceState : Bundle?){
        super.onViewCreated(view, savedInstanceState)

        val args: SecretNumberFragementArgs by navArgs()
        val max = args.max
        val turns = args.turns



        val secretNumberModel : SecretNumberModel by viewModels(
            factoryProducer = {SecretNumberModelFactory(this,this.requireContext(),max,turns)})
        val myToasts = MyToasts(this.requireContext())
        myToasts.setMax(max)
        myToasts.setTurns(turns)

        binding.chooseButton.setOnClickListener {
            secretNumberModel.chooseSecretNumber()
        }

        binding.buttonCheck.setOnClickListener {
            if (binding.editNumber.text.isNotEmpty()) {
                secretNumberModel.check(binding.editNumber.text.toString().toInt())
                if(binding.editNumber.text.toString().toInt() !in 1..max) {
                    myToasts.wrongNum()
                }
                if(secretNumberModel.secretNumber.value !in 1..max){
                    myToasts.noSecretNum()
                }

            }
            else if(binding.editNumber.text.isEmpty()){
                    myToasts.noValue()
            }

        }

        /*  secretNumberModel.secretNumber.observe(this){
              value -> if(value == SecretNumberModel.NO_SECRET_NUMBER){
                  binding.chooseText.visibility = View.GONE
              } else {
                  binding.chooseText.visibility = View.VISIBLE
                  binding.chooseText.text = getString("secret number choosen !", value)
              }

          }*/

        secretNumberModel.secretNumber.observe(viewLifecycleOwner, Observer { value ->
            if(value == SecretNumberModel.NO_SECRET_NUMBER){
                binding.chooseText.visibility = View.GONE
            }
            else {
                binding.chooseText.visibility = View.VISIBLE
                binding.chooseText.text = getString(R.string.chose_text)
                //binding.editNumber.setText(value.toString())
            }

        })

        secretNumberModel.checkResult.observe(viewLifecycleOwner, Observer {value ->
            if(value == SecretNumberModel.CheckResult.NO_GUESS.message){
                binding.resultText.visibility = View.GONE
            }
            else if (value == SecretNumberModel.CheckResult.LOWER.message ){
                binding.resultText.text = getString(R.string.lower_text)
                binding.resultText.visibility = View.VISIBLE
            }
            else if (value == SecretNumberModel.CheckResult.EQUAL.message ){
                binding.resultText.text = getString(R.string.equal_text)
                binding.resultText.visibility = View.VISIBLE
            }
            else if(value == SecretNumberModel.CheckResult.GREATER.message){
                binding.resultText.text = getString(R.string.higher_text)
                binding.resultText.visibility = View.VISIBLE
            }

        })

        secretNumberModel.remainingTurns.observe(viewLifecycleOwner, Observer {value ->
            binding.remainingTurnsText.text = getString(R.string.remaining_turns_text,value)
        })

        secretNumberModel.checkLost.observe(viewLifecycleOwner, Observer {value ->
            if (value == SecretNumberModel.CheckResult.NO_GUESS.message){
                binding.lostText.visibility = View.GONE
            }
            else{
                binding.lostText.text = getString(R.string.lost_text,secretNumberModel.secretNumber.value)
                binding.lostText.visibility = View.VISIBLE
            }
        })

    }
}