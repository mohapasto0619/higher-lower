package fr.mastersid.megasli.higherlower

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import fr.mastersid.megasli.higherlower.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        val navController = findNavController(R.id.nav_host_fragement)
        //val appBarConfiguration = AppBarConfiguration(navController.graph,binding.drawerLayout)
        val  appBarConfiguration = if (binding.drawerLayout  != null) {
            AppBarConfiguration(navController.graph , binding.drawerLayout)
        }
        else {
            AppBarConfiguration(navController.graph)
        }
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)
        binding.navView.setNavigationItemSelectedListener { item  ->
            binding.drawerLayout?.closeDrawers()
            if (item.itemId  == R.id.secretNumberFragementDefault) {
                val  action = NavGraphDirections.actionSettingsFragementToSecretNumberFragementGlobal(100,6)
                findNavController(R.id.nav_host_fragement).navigate(action)
                true
            }
            else {
                item.onNavDestinationSelected(findNavController(R.id.nav_host_fragement))
            }
        }


    }
    override fun onCreateOptionsMenu(menu : Menu?): Boolean{
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true;
    }

    override fun onOptionsItemSelected(item : MenuItem): Boolean {
        return item.onNavDestinationSelected(findNavController(R.id.nav_host_fragement))
    }

}