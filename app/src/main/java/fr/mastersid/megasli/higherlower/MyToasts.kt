package fr.mastersid.megasli.higherlower


import android.content.Context
import android.widget.Toast

class MyToasts(private val context: Context) {

    private var max : Int = 0
    private var turns :Int = 0

    fun noValue(){
        val toast = Toast.makeText(context,context.getString(R.string.no_value_text),Toast.LENGTH_SHORT)
        toast.show()
    }

    fun wrongNum(){
        val toast = Toast.makeText(context,context.getString(R.string.wrong_number_text,max),Toast.LENGTH_SHORT)
        toast.show()
    }

    fun noSecretNum(){
        val toast = Toast.makeText(context,context.getString(R.string.choose_number_text),Toast.LENGTH_SHORT)
        toast.show()
    }

    fun noValueSettings(){
        val toast = Toast.makeText(context,context.getString(R.string.both_value_text),Toast.LENGTH_SHORT)
        toast.show()
    }

    fun greaterNumber(){
        val toast = Toast.makeText(context,context.getString(R.string.greater_number_text),Toast.LENGTH_SHORT)
        toast.show()
    }

    fun savedMax(){
        val toast = Toast.makeText(context,context.getString(R.string.saved_max_text,max),Toast.LENGTH_SHORT)
        toast.show()
    }
    fun savedTurns(){
        val toast = Toast.makeText(context,context.getString(R.string.saved_turns_text,turns),Toast.LENGTH_SHORT)
        toast.show()
    }

    fun loadMax(){
        val toast = Toast.makeText(context,context.getString(R.string.load_max_text),Toast.LENGTH_SHORT)
        toast.show()
    }

    fun loadTurns(){
        val toast = Toast.makeText(context,context.getString(R.string.load_turns_text),Toast.LENGTH_SHORT)
        toast.show()
    }

    fun setMax(maxV : Int){
        max = maxV
    }

    fun setTurns(turnsV : Int){
        turns = turnsV
    }

}