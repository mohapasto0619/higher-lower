package fr.mastersid.megasli.higherlower

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import fr.mastersid.megasli.higherlower.databinding.FragementSecretnumberBinding
import fr.mastersid.megasli.higherlower.databinding.FragementSettingsBinding

class SettingsFragement : Fragment() {

    private lateinit var _binding : FragementSettingsBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container : ViewGroup?,
                              savedInstanceState : Bundle?): View?{
        _binding = FragementSettingsBinding.inflate(inflater)
        return _binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        super.onViewCreated(view, savedInstanceState)

        val settingsFragmentModel : SettingsFragmentModel by viewModels(factoryProducer = {SettingsModelFactory(this.requireContext())})

        val myToasts = MyToasts(this.requireContext())

        _binding.playButton.setOnClickListener {
            if (_binding.maxEditText.text.isNotEmpty() && _binding.turnEditText.text.isNotEmpty()
                    && _binding.maxEditText.text.toString().toInt() > 1) {
                val action = SettingsFragementDirections.actionSettingsFragementToSecretNumberFragement(
                        _binding.maxEditText.text.toString().toInt(), _binding.turnEditText.text.toString().toInt())
                findNavController().navigate(action)
            }
            else if (_binding.maxEditText.text.isNotEmpty() && _binding.turnEditText.text.isNotEmpty()
                    && _binding.maxEditText.text.toString().toInt() <=1){
                myToasts.greaterNumber()
            }
            else{
                myToasts.noValueSettings()
            }
        }

        _binding.saveButton.setOnClickListener {
            if(_binding.maxEditText.text.isNotEmpty()) {
                val max = _binding.maxEditText.text.toString().toInt()
                settingsFragmentModel.save(max)
                myToasts.setMax(max)
                myToasts.savedMax()
            }
            if(_binding.turnEditText.text.isNotEmpty()){
                val turns = _binding.turnEditText.text.toString().toInt()
                settingsFragmentModel.save2(turns)
                myToasts.setTurns(turns)
                myToasts.savedTurns()
            }

        }

        settingsFragmentModel.favorite.observe(viewLifecycleOwner, Observer {value ->
            if(value != SettingsFragmentModel.NO_VALUE && _binding.maxEditText.text.isEmpty()){
                _binding.maxEditText.setText("$value")
                myToasts.loadMax()
            }
        })

        settingsFragmentModel.favorite2.observe(viewLifecycleOwner, Observer {value ->
            if(value != SettingsFragmentModel.NO_VALUE && _binding.turnEditText.text.isEmpty()){
                _binding.turnEditText.setText("$value")
                myToasts.loadTurns()
            }
        })
    }
}