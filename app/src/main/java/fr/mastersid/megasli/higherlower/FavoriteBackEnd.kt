package fr.mastersid.megasli.higherlower

interface FavoriteBackEnd {
    suspend fun saveFavorite(favorite : Int)
    suspend fun saveFavorite2(favorite2 : Int)
    suspend fun loadFavorite():Int
    suspend fun loadFavorite2():Int

    companion object{
        const val NO_VALUE = -1
    }
}