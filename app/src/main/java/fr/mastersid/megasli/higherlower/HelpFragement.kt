package fr.mastersid.megasli.higherlower

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import fr.mastersid.megasli.higherlower.databinding.FragementHelpBinding
import fr.mastersid.megasli.higherlower.databinding.FragementSecretnumberBinding

class HelpFragement : Fragment() {

    private lateinit var binding : FragementHelpBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container : ViewGroup?,
                              savedInstanceState : Bundle?): View?{
        binding = FragementHelpBinding.inflate(inflater)
        return binding.root

    }
}