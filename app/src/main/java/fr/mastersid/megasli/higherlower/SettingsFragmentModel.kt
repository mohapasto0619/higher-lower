package fr.mastersid.megasli.higherlower

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SettingsFragmentModel(private val repository : FavoriteRepository) : ViewModel() {

    val favorite = MutableLiveData(NO_VALUE)
    val favorite2 = MutableLiveData(NO_VALUE)

    init {
        viewModelScope.launch(Dispatchers.IO){
            favorite.postValue(repository.loadFavorite())
            favorite2.postValue(repository.loadFavorite2())
        }
    }

    fun save(max: Int) {
        Log.d("Favorite", "save max = $max")
        viewModelScope.launch(Dispatchers.IO) {
            repository.saveFavorite(max)
            Log.d("Favorite", "end of save")
        }
    }

    fun save2(turns : Int) {
        Log.d("Favorite2", "save turns = $turns")
        viewModelScope.launch(Dispatchers.IO) {
            repository.saveFavorite2(turns)
            Log.d("Favorite2", "end of save")
        }
    }

    companion object{
        const val NO_VALUE = FavoriteRepository.NO_VALUE
    }
}