package fr.mastersid.megasli.higherlower

import android.content.Context
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistry
import androidx.savedstate.SavedStateRegistryOwner

class SecretNumberModelFactory(owner : SavedStateRegistryOwner, private val context : Context, private val max : Int,
                               private val turns : Int) : AbstractSavedStateViewModelFactory(owner, null) {
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
            if (modelClass.isAssignableFrom(SecretNumberModel::class.java)){
                @Suppress("UNCHECKED_CAST")
                return SecretNumberModel(handle, context, max, turns) as T
            }
            throw IllegalArgumentException("Unknown ViewModel Class")
    }
}