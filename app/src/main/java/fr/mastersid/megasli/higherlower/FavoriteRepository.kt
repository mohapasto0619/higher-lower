package fr.mastersid.megasli.higherlower

class FavoriteRepository(private val favoriteBackEnd : FavoriteBackEnd) {

    suspend fun saveFavorite(favorite: Int){
        favoriteBackEnd.saveFavorite(favorite)
    }

    suspend fun saveFavorite2(favorite2: Int){
        favoriteBackEnd.saveFavorite2(favorite2)
    }

    suspend fun loadFavorite():Int{
        return favoriteBackEnd.loadFavorite()
    }

    suspend fun loadFavorite2():Int{
        return favoriteBackEnd.loadFavorite2()
    }

    companion object {
        const val NO_VALUE = FavoriteBackEnd.NO_VALUE
    }
}